COMPILER=ocamlfind ocamlc
EXE=a.out

main: api monadic_parser.cmi monadic_parser.cmo
	$(COMPILER) str.cma monadic_parser.cmo exemple.ml


api: monadic_parser.ml
	$(COMPILER) str.cma monadic_parser.ml 
	
clean:
	rm ./*.cm*
