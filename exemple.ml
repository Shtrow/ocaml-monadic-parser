open Monadic_parser

let comp op exp1 exp2 = 
    match op with
    `Plus -> exp1 + exp2
    |`Minus -> exp1 - exp2
    |`Product -> exp1 * exp2

let op = 
    let* s = (char '+') <|>
    (char '-' )<|>
    (char '*' )in 
    match s with
    |'+' -> return `Plus
    |'-' -> return `Minus
    |'*' -> return `Product
    |_ -> return `Plus


(* GRAMMAR
 *
 * expr := term + expr | term
 * term := factor * term | factor
 * factor := (expr) | int
 * *)



let rec expr s= 
    begin
        begin
    let* x = term in 
    let* _ = char '+' in
    let* y = expr in 
    return (x+y)
        end
    <|> term
    end s

and term s= 
    begin
        begin
        let* x= factor in 
        let* _= char '*' in
        let* y= expr in
        return (x*y)
        end
    <|> factor
    end s

and factor s = 
    begin
        begin
        let* _ = char '(' in 
        let* x = expr in
        let* _ = char ')' in 
        return x
    end
        <|> int
    end s
;;
let input = "2+3*9" in
Printf.printf "INPUT -> %s \nOUTPUT -> " input;
List.iter (fun (i,s)-> print_int i)(expr input);;
