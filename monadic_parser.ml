(**
 * Le parser de Vio
 * De qualitée fermière
 * 
 * I tried to implement this from that video https://youtu.be/dDtZLm7HIJs
 *
 *)
type 'a parser = string -> ('a * string) list

let join (p : 'a parser parser) : 'a parser = 
    fun s -> let mma = p s in 
    List.map (fun (p,s) -> p s) mma
    |> List.flatten

let map (f : 'a -> 'b) (p : 'a parser) : 'b parser = 
    fun s -> let ma = p s in 
    List.map (fun (a,st) -> (f a,st)) ma

let return v :'a parser = fun s -> [(v,s)]

let bind p f = join (map f p)

let (let*) = bind

let (>>=) = bind

let (>=>) p1 p2 = 
    fun x -> (p1 x) >>= p2

let empty = fun s -> []

(* p1 <|> p2 returns p2 if p1 did fail, else p1 *)
and (<|>) (p1) (p2) = 
    fun s -> 
        let v = p1 s in 
        match v with
        | [] -> p2 s
        | _ -> p1 s

(** Eat zero or more char *)
let rec many p=
    fun s ->
        let v = p s in
        match v with 
        |[] -> [("",s)]
        | _ ->  begin
            let* s1 = p in
            let* s2 = many p in
            let n_s = (String.make 1 s1) in
            return (n_s^s2)
        end s

(** Eat one or more char *)
let rec some p =
    let* s = many p in
    match s with
    | "" -> empty
    | _ -> return s

(* seq p1 p2 returns a parser that first parse with p1 then p2. Only keep the result of p2 *)
let seq p1 p2 = 
    let* _ = p1 in
    let* s = p2 in
    return s

let is_digit digit =
    match digit with
     '0' .. '9' -> true
    | _ -> false

let is_letter l =
    match l with
     'a' .. 'z' -> true
    | _ -> false

let is_space l =
    match l with
    ' ' -> true
    |_ -> false


let item : char parser = function 
    | "" -> []
    | s -> try
       match String.get s 0 with
        | c -> [( c),String.sub s 1 (String.length s -1)]
    with
    | _ -> []

(* sat f returns a parser that parse one char c that satisfied f c *)
let sat f =
    let* x = item in
        if f x then return x else empty

(*let tmp_pars f s = *)
    (*try*)
       (*match String.get s 0 with*)
        (*| c when f c -> [( c),String.sub s 1 (String.length s -1)]*)
        (*| _ -> []*)
    (*with*)
    (*| _ -> []*)

let digit = sat is_digit

let letter =sat is_letter

let char c = sat (Char.equal c)

let nat = 
    let* x = some digit in 
     return (int_of_string x)

let int = 
    (let* _ = char '-' in
    let* x = nat in 
    return (-x))
    <|> nat

let float = 
    let* int_part = int in 
    let* _ = char '.' in
    let* s_nat_part = some digit in
    let s_int_part = string_of_int int_part in
    return (float_of_string (s_int_part^"."^s_nat_part))

let white = 
    let* _ = many (sat is_space) in 
    return ()

let end_of_file = 
    some (char ' ')

let from_regex (reg : Str.regexp) = 
    (fun s -> 
        let i = Str.search_forward reg s 0 in
        let m = String.sub s i (String.length s) in
        return m s)
